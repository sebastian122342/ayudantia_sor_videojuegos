#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
  pid_t pid;
  time_t t;
  int estado;

  if ((pid = fork()) < 0)
     perror("Error de fork()\n");

  else if (pid == 0) { //Soy el Hijo
     printf("Hijoooooooooo %d\n", getpid());
     sleep(5);
     exit(155);
  }else {
     /* printf("Soy el Papá!\n");
      waitpid(pid, &estado, 0);
      printf("Hijo listo: %d!!!\n", estado);
      */

       do {
          //WNOHANG retorna de inmediato estado del proceso!
          if ((pid = waitpid(pid, &estado, WNOHANG)) == -1)
              perror("Error en función wait()\n");

          else if (pid == 0) { //Proceso Hijo aún en Ejecución
              time(&t);  //Hora Actual
              printf("Proceso Hijo está aún en Ejecución en %s", ctime(&t));
              sleep(1);

          }else { //Proceso Hijo Listo!
            if (WIFEXITED(estado))
                printf("Proceso Hijo Terminó con Estado %d\n", WEXITSTATUS(estado));

            else 
                printf("Proceso Hijo No Termina de forma Existosa\n");
          }
        } while (pid == 0);

  }


  /*
  */
}