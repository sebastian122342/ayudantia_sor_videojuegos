#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
  char *const argumentos[] = {"find", ".", NULL};
  
  int pid = fork();

  printf("PID: %d, IDP: %d\n", pid, getpid());

  if ( pid == 0 ) {
    execvp( "find", argumentos);
  
  
  // Put the parent to sleep for 2 seconds--let the child finished executing 
  sleep(2);
  
  printf( "El Padre Termina!!!!\n- El Hijo Nunca Pasa por Acá!!!\n" );
  
  return 0;
}
