#include<stdio.h>
#include<unistd.h>

int main(int argc, char *argv[]){
    printf("Soy %s, y mi PID es %d\n", argv[0], getpid());

    
    // int execv(const char *path, char *const argv[]);
    char *const argumentos[] = {"tarea ejemplo", "arg1", "arg2", NULL};

    execv("./lab1", argumentos);

    printf("Error de Ejecución!\n");
    return -1;

}
