#include<stdio.h>
#include<unistd.h>

int main(int nargs, char *argumentos[]){
    printf("Soy %s y mi PID es %d\n", argumentos[0], getpid());

    for(int i=0; i< nargs; i++){
        printf("argumento %d: %s\n", i, argumentos[i]);
    }

}

