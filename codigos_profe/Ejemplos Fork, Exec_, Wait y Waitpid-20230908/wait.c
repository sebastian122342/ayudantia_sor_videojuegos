#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
  pid_t pid;
  time_t t;
  int estado;

  if ((pid = fork()) < 0)
     perror("Error de fork()\n");

  else if (pid == 0) { //Soy el Hijo
     printf("Hijoooooooooo %d\n", getpid());
     sleep(15);
     exit(155);
  }else {
      pid_t pid2 = fork();

      if (pid2 == 0) //Nuevo hijo
      {
        printf("Hijo 2 PID %d\n", getpid());
        exit(1);
      }
      else{
        pid_t pid3 = wait(NULL);
        printf("El Hijo %d Terminó\n", pid3);

//        pid_t pid4 = wait(NULL);
//        printf("El Hijo %d Terminó\n", pid4);


      }
  }


  /*
  */
}